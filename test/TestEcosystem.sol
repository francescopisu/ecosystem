pragma solidity ^0.4.11;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/Ecosystem.sol";

contract TestEcosystem {

//    uint totalPlayers = 0;
//    uint matchTime = 60 minutes;
//    uint matchCost = 0.1 ether;

    function testInitialStatusUsingDeployedContract() {
        Ecosystem e = Ecosystem(DeployedAddresses.Ecosystem());

        bytes32 hash = e.createAdherent('prova', 'a');
        Assert.isNotZero(hash, "hash should not be empty");
    }


//    function testInitialStatusWithNewEcosystem() {
//        Ecosystem e = new Ecosystem();
//
//        Assert.equal(e.totalPlayers(), totalPlayers, "Initial players should be initially 0");
//        Assert.equal(e.matchTime(), matchTime, "One match should last an hour");
//        Assert.equal(e.matchCost(), matchCost, "One match should cost 0.1 ether");
//    }

}

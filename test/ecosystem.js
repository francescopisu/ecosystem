/**
 * Created by francescopisu on 21/06/17.
 */
var Ecosystem = artifacts.require('./Ecosystem.sol'),
    Web3 = require('web3')

var web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'))

contract('Ecosystem', function (accounts) {

    it('hash should be an hash', function () {
        var e, hash

        return Ecosystem.deployed().then(function (instance) {
            e = instance
            return e.createAdherent.call('ciao', 'a', 'aa', 'aaa')
        }).then(function (hash) {
            console.log(typeof(hash))
            return e.getAdherentByHash.call(hash+'')
        }).then(function (res) {
            //assert.equal(hash, 0, 'Initial players should be 0')
            console.log(res)
        })
    })

    // it('It should create a unique big number of the sign', function () {
    //     return RockPaperScissors.deployed().then(function (instance) {
    //         return instance.createSign.call(1)
    //     }).then(function (bigNumber) {
    //         var sign = web3.toBigNumber(bigNumber).modulo(3).plus(1).toNumber()
    //         assert.equal(sign, 1, 'The sign is "Scissors"')
    //     })
    // })
    //
    // it('should call a function that depends on a linked library', function () {
    //     var meta
    //     var metaCoinBalance
    //     var metaCoinEthBalance
    //
    //     return MetaCoin.deployed().then(function (instance) {
    //         meta = instance
    //         return meta.getBalance.call(accounts[0])
    //     }).then(function (outCoinBalance) {
    //         metaCoinBalance = outCoinBalance.toNumber()
    //         return meta.getBalanceInEth.call(accounts[0])
    //     }).then(function (outCoinBalanceEth) {
    //         metaCoinEthBalance = outCoinBalanceEth.toNumber()
    //     }).then(function () {
    //         assert.equal(metaCoinEthBalance, 2 * metaCoinBalance, 'Library function returned unexpected function, linkage may be broken')
    //     })
    // })

    // it('should send coin correctly', function () {
    //     var meta
    //
    //     //    Get initial balances of first and second account.
    //     var account_one = accounts[0]
    //     var account_two = accounts[1]
    //
    //     var account_one_starting_balance
    //     var account_two_starting_balance
    //     var account_one_ending_balance
    //     var account_two_ending_balance
    //
    //     var amount = 10
    //
    //     return MetaCoin.deployed().then(function (instance) {
    //         meta = instance
    //         return meta.getBalance.call(account_one)
    //     }).then(function (balance) {
    //         account_one_starting_balance = balance.toNumber()
    //         return meta.getBalance.call(account_two)
    //     }).then(function (balance) {
    //         account_two_starting_balance = balance.toNumber()
    //         return meta.sendCoin(account_two, amount, {from: account_one})
    //     }).then(function () {
    //         return meta.getBalance.call(account_one)
    //     }).then(function (balance) {
    //         account_one_ending_balance = balance.toNumber()
    //         return meta.getBalance.call(account_two)
    //     }).then(function (balance) {
    //         account_two_ending_balance = balance.toNumber()
    //
    //         assert.equal(account_one_ending_balance, account_one_starting_balance - amount, 'Amount wasn\'t correctly taken from the sender')
    //         assert.equal(account_two_ending_balance, account_two_starting_balance + amount, 'Amount wasn\'t correctly sent to the receiver')
    //     })
    // })
})

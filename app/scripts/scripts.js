/**
 * Created by francescopisu on 23/06/17.
 */
var aboutDiv = document.getElementById('about')
var admissionDiv = document.getElementById('admission')
var pendingDiv = document.getElementById('pendingList')
var errorDiv = document.getElementById('services')
var loginDiv = document.getElementById('login')

var admissionToggle = document.getElementById('admission-toggle')
var aboutToggle = document.getElementById('about-toggle')
var pendingToggle = document.getElementById('pending-toggle')
var servicesToggle = document.getElementById('services-toggle')
var loginToggle = document.getElementById('login-toggle')


var admissionLi = document.getElementById('admission-li')
var aboutLi = document.getElementById('about-li')
var pendingLi = document.getElementById('pending-li')
var servicesLi = document.getElementById('services-li')
var loginLi = document.getElementById('login-li')

admissionToggle.addEventListener('click', function (event) {
    aboutDiv.style.display = 'none'
    //admissionDiv.style.display = 'none'
    pendingDiv.style.display = 'none'
    errorDiv.style.display = 'none'
    loginDiv.style.display = 'none'
    aboutLi.classList.remove('active')
    pendingLi.classList.remove('active')

    loginLi.classList.remove('active')
    admissionLi.classList.toggle('active')
})

aboutToggle.addEventListener('click', function (event) {
    admissionDiv.style.display = 'none'
    aboutDiv.style.display = 'block'
    pendingDiv.style.display = 'none'
    errorDiv.style.display = 'none'
    loginDiv.style.display = 'none'
    admissionLi.classList.remove('active')
    pendingLi.classList.remove('active')
    loginLi.classList.remove('active')
    aboutLi.classList.toggle('active')
})

pendingToggle.addEventListener('click', function (event) {
    admissionDiv.style.display = 'none'
    aboutDiv.style.display = 'none'
    errorDiv.style.display = 'none'
    loginDiv.style.display = 'none'
    pendingDiv.style.display = 'block'
    admissionLi.classList.remove('active')
    aboutLi.classList.remove('active')
    loginLi.classList.remove('active')
    pendingLi.classList.toggle('active')
})

loginToggle.addEventListener('click', function (event) {
    admissionDiv.style.display = 'none'
    aboutDiv.style.display = 'none'
    errorDiv.style.display = 'none'
    pendingDiv.style.display = 'none'
    loginDiv.style.display = 'inline-block'
    admissionLi.classList.remove('active')
    aboutLi.classList.remove('active')
    loginLi.classList.toggle('active')
    pendingLi.classList.remove('active')
})

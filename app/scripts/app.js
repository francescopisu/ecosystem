// Import the page's CSS. Webpack will know what to do with it.
import '../layout.css'

// Import libraries we need.
import { default as Web3 } from 'web3'
import { default as contract } from 'truffle-contract'

// Import our contract artifacts and turn them into usable abstractions.
import ecoArtifacts from '../../build/contracts/Ecosystem.json'

// MetaCoin is our usable abstraction, which we'll use through the code below.
let Ecosystem = contract(ecoArtifacts)

// The following code is simple to show off interacting with your contracts.
// As your needs grow you will likely need to change its form and structure.
// For application bootstrapping, check out window.addEventListener below.
let accounts, account, web3, id = 0, dim, elem, current = 0

let admin = false // true -> admin, false -> normal User
let loggedIn = false
let loginError = false
let authorized = false

let users = {
    user: { username: 'user', password: '1111' },
    admin: { username: 'admin', password: '0000' }
}

let dom = {
    name: document.getElementById('name'),
    surname: document.getElementById('surname'),
    FC: document.getElementById('FC'),
    VAT: document.getElementById('VAT'),
    status: document.getElementById('status'),
    pendingList: document.getElementById('userList-div'),
    user: document.getElementById('user'),
    psw: document.getElementById('psw'),
    login: document.getElementById('internal-login'),
    logout: document.getElementById('logout'),
    admission: document.getElementById('admission'),
    errorLogin: document.getElementById('error-login')
}

window.getUser = function (id) {
    Ecosystem.deployed().then(function (instance) {
        return instance.retrieveUser(id, { from: web3.eth.accounts[0] })
    }).then(function (res) {
        console.log(res[0])
    })
}

window.getAcceptedUser = function (id) {
    Ecosystem.deployed().then(function (instance) {
        return instance.retrieveAcceptedUser(id, { from: web3.eth.accounts[0] })
    }).then(function (res) {
        console.log(res)
    })
}

window.getRejectedUser = function (id) {
    Ecosystem.deployed().then(function (instance) {
        return instance.retrieveRejectedUser(id, { from: web3.eth.accounts[0] })
    }).then(function (res) {
        console.log(res)
    })
}

window.getSize = function () {
    Ecosystem.deployed().then(function (instance) {
        return instance.getUserIdsSize({ from: web3.eth.accounts[0] })
    }).then(function (res) {
        console.log('size:' + res.toString())
    })
}

window.isAuthorized = function () {
    Ecosystem.deployed().then(function (instance) {
        return instance.isAnAddressAuthorized(account, { from: web3.eth.accounts[0] })
    }).then(function (res) {
        console.log('authorized:' + res.toString())
    })
}

window.App = {
    start: function () {
        let self = this

        // Bootstrap the MetaCoin abstraction for Use.
        Ecosystem.setProvider(web3.currentProvider)

        // Get the initial account balance so it can be displayed.
        web3.eth.getAccounts(function (err, accs) {
            if (err !== null) {
                window.alert('There was an error fetching your accounts.')
                return
            }

            if (accs.length === 0) {
                window.alert('Couldn\'t get any accounts! Make sure your Ethereum client is configured correctly.')
                return
            }

            accounts = accs
            account = accounts[0]
        })
    },

    setStatus: function (message) {
        dom.status.innerHTML = message
    },

    admissionForm: function () {
        if (admin) {
            dom.admission.style.display = 'none'
        } else dom.admission.style.display = 'inline-block'
    },

    /* Il login potrà essere effettuato solo utilizzando username e password che verranno scelte/fornite in seguito
    all'esito positivo della richiesta di ammissione all'ecosistema.
    Quindi inizialmente il form di login sarà visibile ma un utente che per la prima volta arriva nella pagina non
    sarà possibile effettuare il login; dovrà infatti richiedere l'ammissione all'ecosistema. Una volta approvata
    riceverà via mail (ad esempio) uno username e una password temporanea che verrà resettata al primo login */
    login: function () {
        let username = dom.user.value
        let password = dom.psw.value

        if (username == users.admin.username && password == users.admin.password) {
            admin = true
            loggedIn = true
            dom.errorLogin.style.display = 'none'
            dom.login.style.display = 'none'
            dom.logout.style.display = 'block'
        } else if (username == users.user.username && password == users.user.password) {
            admin = false
            loggedIn = true
            dom.errorLogin.style.display = 'none'
            dom.login.style.display = 'none'
            dom.logout.style.display = 'block'
        } else {
            dom.errorLogin.style.display = 'block'
        }
    },

    logout: function () {
        loggedIn = false
        admin = false
        dom.login.style.display = 'block'
        dom.logout.style.display = 'none'
    },

    createUser: function () {
        let self = this

        let name = dom.name.value
        let surname = dom.surname.value
        let FC = dom.FC.value
        let VAT = dom.VAT.value

        let eco
        this.setStatus('Initiating transaction... (please wait)')
        id++
        Ecosystem.deployed().then(function (instance) {
            eco = instance
            return eco.createUser(id - 1, name, surname, FC, VAT, { from: account, gas:1000000, value: web3.toWei('1', 'ether') })
        }).then(function () {
            return eco.retrieveUser(id - 1, { from: account })
        }).then(function (r2) {
            self.setStatus('Success: user has been added!')
            console.log(r2)
        }).catch(function (e) {
            self.setStatus('Error while adding user')
            console.log(e)
        })
    },

    viewWaitingList: function () {
        let self = this

        let eco
        if (admin) {
            document.getElementById('error-userList').style.display = 'none'
            // Recupero la dimensione dell'array di ids
            Ecosystem.deployed().then(function (instance) {
                eco = instance
                return eco.getUserIdsSize({ from: account })
            }).then(function (res) {
                dim = parseInt(res.toString())
                //  dom.pendingList.innerHTML = ''

                console.log('current:' + current)

                for (let i = current; i < dim; i++) {
                    current = i + 1
                    console.log('current:' + current)
                    Ecosystem.deployed().then(function (instance) {
                        eco = instance
                        return eco.retrieveUser(i, { from: account })
                    }).then(function (res) {
                        elem = res
                        let newtr = document.createElement('tr')

                        let tdNome = document.createElement('td')
                        let text1 = document.createTextNode(elem[0])
                        tdNome.appendChild(text1)
                        newtr.appendChild(tdNome)

                        let tdCognome = document.createElement('td')
                        let text2 = document.createTextNode(elem[1])
                        tdCognome.appendChild(text2)
                        newtr.appendChild(tdCognome)

                        let tdFC = document.createElement('td')
                        let text3 = document.createTextNode(elem[2])
                        tdFC.appendChild(text3)
                        newtr.appendChild(tdFC)

                        let tdVAT = document.createElement('td')
                        let text4 = document.createTextNode(elem[3])
                        tdVAT.appendChild(text4)
                        newtr.appendChild(tdVAT)

                        let tdStatus = document.createElement('td')
                        tdStatus.setAttribute('id', 'status' + i)
                        let text5 = document.createTextNode('Pending')
                        tdStatus.appendChild(text5)
                        newtr.appendChild(tdStatus)

                        let tdAccetta = document.createElement('td')
                        let buttonA = document.createElement('button')
                        buttonA.innerHTML = '&#10004;'
                        buttonA.setAttribute('id', 'buttonA' + i)
                        tdAccetta.appendChild(buttonA)
                        newtr.appendChild(tdAccetta)

                        let tdRifiuta = document.createElement('td')
                        let buttonR = document.createElement('button')
                        buttonR.innerHTML = '&#10008;'
                        buttonR.setAttribute('id', 'buttonR' + i)
                        tdRifiuta.appendChild(buttonR)
                        newtr.appendChild(tdRifiuta)

                        document.getElementById('tableBody').appendChild(newtr)

                        document.getElementById('buttonA' + i).onclick = function () { // Accetta
                            window.App.manageUser(i, true)
                            document.getElementById('buttonA' + i).style.display = 'none'
                            document.getElementById('buttonR' + i).style.display = 'none'
                            document.getElementById('status' + i).innerText = 'Accepted'
                        }

                        document.getElementById('buttonR' + i).onclick = function () {
                            window.App.manageUser(i, false)
                            document.getElementById('buttonR' + i).style.display = 'none'
                            document.getElementById('buttonA' + i).style.display = 'none'
                            document.getElementById('status' + i).innerText = 'Rejected'
                        }
                    }).catch(function (e) {
                        self.setStatus('Error while adding user')
                        console.log(e)
                    })
                }
                // document.getElementById('userList').appendChild(tbody)
                document.getElementById('userList-div').style.display = 'block'
            }).catch(function (e) {
                self.setStatus('Error while adding user')
                console.log(e)
            })
        } else { // se non sei admin, non vedi la lista
            console.log('admin è a false')
            document.getElementById('error-userList').style.display = 'block'
            document.getElementById('userList-div').style.display = 'none'
        }
    },

    manageUser: function (id, flag) {
        let self = this
        let eco
        Ecosystem.deployed().then(function (instance) {
            eco = instance
            return eco.manageUser(id, flag, { from: account, gas:1000000 })
        }).then(function () {
            self.setStatus('Success: user accepted!')
        }).catch(function (e) {
            self.setStatus('User has not been accepted')
            console.log(e)
        })
    }

}

window.addEventListener('load', function () {
    // Checking if Web3 has been injected by the browser (Mist/MetaMask)
    if (typeof web3 !== 'undefined') {
        console.warn('Using web3 detected from external source. If you find that your accounts don\'t ' +
            'appear or you have 0 MetaCoin, ensure you\'ve configured that source properly. If ' +
            'using MetaMask, see the following link. Feel free to delete this warning. :) ' +
            'http://truffleframework.com/tutorials/truffle-and-metamask')
        // Use Mist/MetaMask's provider
        web3 = new Web3(web3.currentProvider)
    } else {
        console.warn('No web3 detected. Falling back to http://localhost:8545. You should ' +
            'remove this fallback when you deploy live, as it\'s inherently insecure. ' +
            'Consider switching to Metamask for development. More info here: ' +
            'http://truffleframework.com/tutorials/truffle-and-metamask')
        // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
        web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'))
    }

    window.App.start()
})

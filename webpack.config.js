const path = require('path')
const CopyWebpackPlugin = require('copy-webpack-plugin')
// const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    entry: {
        'app': './app/scripts/app.js',
        'scripts': './app/scripts/scripts.js'
    },

    output: {
        path: path.resolve(__dirname, 'build'),
        filename: '[name].js'
    },
    plugins: [
        // Copy our app's about.html to the build folder.
        new CopyWebpackPlugin([
            { from: './app/assets/index.html', to: 'index.html' }
        ])
        /* new HtmlWebpackPlugin({
            filename: 'about.html',
            template: './app/assets/about.html',
            inject: 'body'
        }),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: './app/assets/index.html',
            inject: 'body'
        }),
        new HtmlWebpackPlugin({
            filename: 'menu.html',
            template: './app/assets/menu.html',
        }) */
    ],
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            }
        ],
        loaders: [
            { test: /\.json$/, use: 'json-loader' },
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015'],
                    plugins: ['transform-runtime']
                }
            }
        ]
    }
}

pragma solidity ^0.4.11;

contract Ecosystem {
    struct User {
        uint id;
        string name;
        string surname;
        string FC;
        string VATnumber;
        address creator;
    }



    // events
    event userCreated(address from, uint id);

    // mappings
    mapping(uint => User) public waitingList;
    mapping(uint => User) public accepted;
    mapping(uint => User) public rejected;

    // simple arrays
    uint[] userIds;
    mapping(address => bool) public authorizedUsers;


    function createUser(uint _id, string _name, string _surname, string _FC, string _VATnumber) payable {
        User memory u = User(_id, _name, _surname, _FC, _VATnumber, msg.sender);
        // Salvo lo User in posizione _id
        waitingList[_id] = u;

        userIds.push(_id);
        userCreated(msg.sender, _id);
    }

    function retrieveUser(uint id) constant returns (string, string, string, string) {
        User memory u = waitingList[id];
        return (u.name, u.surname, u.FC, u.VATnumber);
    }

    function retrieveAcceptedUser(uint id) constant returns (string, string, string, string) {
        User memory u = accepted[id];
        return (u.name, u.surname, u.FC, u.VATnumber);
    }

    function retrieveRejectedUser(uint id) constant returns (string, string, string, string) {
        User memory u = rejected[id];
        return (u.name, u.surname, u.FC, u.VATnumber);
    }

    function getUserIdsSize() constant returns (uint) {
        return userIds.length;
    }

    /**
     *  manageUser: accepts of rejects a user specified by it's id depending on flag's value
     *  @param id the user's id
     *  @param flag true/false flag
     */
    function manageUser(uint id, bool flag) {
        // Retrieve user by it's id
        User memory u = waitingList[id];
        if(flag) { // flag == true => accept user
            accepted[id] = u;
            authorizedUsers[u.creator] = true;
        } else {
            rejected[id] = u;
        }
     }

    function isAnAddressAuthorized(address addr) constant returns (bool) {
        if(authorizedUsers[addr] == true) {
            return true;
        }
        return false;
    }

}

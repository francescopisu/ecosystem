# Ethereum Dapp
Example webpack project with Truffle. Includes contracts, migrations, tests, user interface and webpack build pipeline.

## Build and start frontend

1. First run `npm run testrpc` to start a preconfigured ethereum network
1. Then run `npm run deploy-contracts`, to deploy the contracts onto your network of choice (default "development").
1. Then run `npm start` to build the app and serve it on http://localhost:8080

## Other npm scripts

If you want to use mist and geth scripts download them from 
[Mist releases](https://github.com/ethereum/mist/releases) and
[Geth releases](https://geth.ethereum.org/downloads/)


